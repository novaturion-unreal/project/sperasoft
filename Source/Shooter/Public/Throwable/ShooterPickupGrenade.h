﻿#pragma once

#include "CoreMinimal.h"
#include "GameFramework/Actor.h"
#include "ShooterPickupGrenade.generated.h"


class UShooterPickUpComponent;
class UShooterGrenadeComponent;
class UProjectileMovementComponent;
class URadialForceComponent;


UCLASS()
class SHOOTER_API AShooterPickupGrenade : public AActor
{
	GENERATED_BODY()

public:
	AShooterPickupGrenade();

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Pickup)
	UShooterPickUpComponent* PickUpComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Grenade)
	UShooterGrenadeComponent* GrenadeComponent;
};
