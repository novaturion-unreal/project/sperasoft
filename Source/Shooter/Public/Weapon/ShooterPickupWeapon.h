﻿#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"

#include "ShooterPickupWeapon.generated.h"


class UShooterWeaponComponent;
class UShooterPickUpComponent;


UCLASS()
class SHOOTER_API AShooterPickupWeapon : public AActor
{
	GENERATED_BODY()

public:
	AShooterPickupWeapon();

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Pickup)
	UShooterPickUpComponent* PickUpComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Weapon)
	UShooterWeaponComponent* WeaponComponent;
};
