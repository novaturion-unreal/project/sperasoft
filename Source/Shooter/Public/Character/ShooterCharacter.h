// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "InputActionValue.h"

#include "GameFramework/Character.h"

#include "ShooterCharacter.generated.h"

class UAnimMontage;
class UCameraComponent;
class UInputAction;
class UInputComponent;
class UInputMappingContext;
class USceneComponent;
class UShooterHealthComponent;
class USkeletalMeshComponent;
class USoundBase;


UCLASS(config=Game)
class AShooterCharacter : public ACharacter
{
	GENERATED_BODY()

public:
	AShooterCharacter();

public:
	bool GetHasRifle() const
	{
		return bHasRifle;
	}

	USkeletalMeshComponent* GetFirstPersonMeshComponent() const
	{
		return FirstPersonMeshComponent;
	}

	UCameraComponent* GetFirstPersonCameraComponent() const
	{
		return FirstPersonCameraComponent;
	}

public:
	void SetHasRifle(
		const bool bNewHasRifle
	)
	{
		bHasRifle = bNewHasRifle;
	}

protected:
	virtual void BeginPlay() override;

protected:
	void Move(
		const FInputActionValue& Value
	);

	void Look(
		const FInputActionValue& Value
	);

	void Pause(
		const FInputActionValue& Value
	) const;

protected:
	// APawn interface

	virtual void SetupPlayerInputComponent(
		UInputComponent* PlayerInputComponent
	) override;

	// End of APawn interface

protected:
	/** Pawn mesh: 1st person view (arms; seen only by self) */
	UPROPERTY(VisibleDefaultsOnly, Category=Mesh)
	USkeletalMeshComponent* FirstPersonMeshComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Camera)
	UCameraComponent* FirstPersonCameraComponent;

	UPROPERTY(VisibleAnywhere, BlueprintReadOnly, Category=Health)
	UShooterHealthComponent* HealthComponent;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputMappingContext* DefaultMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* JumpAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* LookAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* MoveAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* PauseAction;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Hud)
	TSubclassOf<UUserWidget> PauseMenuWidgetClass;

	UPROPERTY()
	UUserWidget* PauseMenuWidget;

protected:
	UPROPERTY(VisibleAnywhere, BlueprintReadWrite, Category=Weapon)
	bool bHasRifle;
};
