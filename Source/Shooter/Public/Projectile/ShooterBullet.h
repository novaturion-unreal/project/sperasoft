﻿#pragma once

#include "CoreMinimal.h"
#include "ShooterProjectile.h"
#include "ShooterBullet.generated.h"


UCLASS()
class SHOOTER_API AShooterBullet : public AShooterProjectile
{
	GENERATED_BODY()

public:
	AShooterBullet();

public:
	UFUNCTION()
	void OnHit(
		UPrimitiveComponent* HitComp,
		AActor* OtherActor,
		UPrimitiveComponent* OtherComp,
		FVector NormalImpulse,
		const FHitResult& Hit
	);
};
