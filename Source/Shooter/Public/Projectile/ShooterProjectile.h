// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "GameFramework/Actor.h"

#include "ShooterProjectile.generated.h"

class USphereComponent;
class UProjectileMovementComponent;


UCLASS(config=Game)
class AShooterProjectile : public AActor
{
	GENERATED_BODY()

public:
	AShooterProjectile();

public:
	USphereComponent* GetCollisionComponent() const
	{
		return CollisionComponent;
	}

	UProjectileMovementComponent* GetProjectileMovementComponent() const
	{
		return ProjectileMovement;
	}

	UStaticMeshComponent* GetMeshComponent() const
	{
		return MeshComponent;
	}

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Collision)
	USphereComponent* CollisionComponent;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Movement)
	UProjectileMovementComponent* ProjectileMovement;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Mesh)
	UStaticMeshComponent* MeshComponent;
};
