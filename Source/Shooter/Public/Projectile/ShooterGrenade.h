﻿#pragma once

#include "CoreMinimal.h"
#include "ShooterProjectile.h"
#include "ShooterGrenade.generated.h"


class URadialForceComponent;


UCLASS()
class SHOOTER_API AShooterGrenade : public AShooterProjectile
{
	GENERATED_BODY()

public:
	AShooterGrenade();

public:
	virtual void Destroyed() override;

public:
	URadialForceComponent* GetRadialForceComponent() const
	{
		return RadialForceComponent;
	}

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Explosion)
	URadialForceComponent* RadialForceComponent;
};
