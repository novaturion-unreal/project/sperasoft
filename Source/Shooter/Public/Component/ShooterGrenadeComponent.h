﻿#pragma once

#include "CoreMinimal.h"
#include "ShooterGrenadeComponent.generated.h"


class USphereComponent;
class UProjectileMovementComponent;
class UInputAction;
class UInputMappingContext;
class URadialForceComponent;


UCLASS()
class SHOOTER_API UShooterGrenadeComponent : public UStaticMeshComponent
{
	GENERATED_BODY()

public:
	UShooterGrenadeComponent();

public:
	UFUNCTION(BlueprintCallable, Category="Grenade")
	void Attach(
		class AShooterCharacter* TargetCharacter
	);

	UFUNCTION(BlueprintCallable, Category="Grenade")
	void PredictTrajectory();

	UFUNCTION(BlueprintCallable, Category="Grenade")
	void Throw();

protected:
	virtual void EnableGrenade();

	virtual void DisableGrenade();

	virtual void ExplodeGrenade();

protected:
	virtual void BeginPlay() override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Projectile)
	TSubclassOf<class AShooterGrenade> ProjectileClass;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Gameplay)
	FVector HandOffset;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputMappingContext* GrenadeMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* ThrowAction;

private:
	UPROPERTY()
	AShooterGrenade* Projectile;
};
