// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/ActorComponent.h"
#include "ShooterHealthComponent.generated.h"


UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class SHOOTER_API UShooterHealthComponent : public UActorComponent
{
	GENERATED_BODY()

public:
	// Sets default values for this component's properties
	UShooterHealthComponent();

public:
	// Called every frame
	virtual void TickComponent(
		float DeltaTime,
		ELevelTick TickType,
		FActorComponentTickFunction* ThisTickFunction
	) override;

public:
	UFUNCTION(BlueprintPure)
	virtual float GetHealth() const;

public:
	UFUNCTION(BlueprintCallable)
	virtual float DecreaseHealth(
		float Amount
	);

	UFUNCTION(BlueprintCallable)
	virtual float IncreaseHealth(
		float Amount
	);

protected:
	// Called when the game starts
	virtual void BeginPlay() override;

protected:
	static inline constexpr float MaxHealth = 100.0f;

	UPROPERTY(BlueprintReadOnly, Category=Health)
	bool bIsImmortal = false;

	UPROPERTY(BlueprintReadOnly, Category=Health, meta=(ClampMin="0.0", ClampMax="100.0", EditCondition="!bIsImmortal"))
	float Health = MaxHealth;
};
