// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"

#include "Components/SkeletalMeshComponent.h"

#include "ShooterWeaponComponent.generated.h"


class AShooterProjectile;
class AShooterCharacter;
class UInputAction;
class UInputMappingContext;


UCLASS(Blueprintable, BlueprintType, ClassGroup=(Custom), meta=(BlueprintSpawnableComponent))
class SHOOTER_API UShooterWeaponComponent : public USkeletalMeshComponent
{
	GENERATED_BODY()

public:
	UShooterWeaponComponent();

public:
	UFUNCTION(BlueprintPure)
	int32 GetAmmoCount() const;

	UFUNCTION(BlueprintPure)
	int32 GetMaxAmmoCount() const;

public:
	UFUNCTION(BlueprintCallable, Category="Weapon")
	void Attach(
		AShooterCharacter* TargetCharacter
	);

	UFUNCTION(BlueprintCallable, Category="Weapon")
	void Fire();

	UFUNCTION(BlueprintCallable, Category="Weapon")
	void Reload();

protected:
	UFUNCTION()
	virtual void EndPlay(
		const EEndPlayReason::Type EndPlayReason
	) override;

protected:
	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Projectile)
	TSubclassOf<AShooterProjectile> ProjectileClass;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	FVector MuzzleOffset;


	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	USoundBase* FireSound;

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category=Gameplay)
	UAnimMontage* FireAnimation;


	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputMappingContext* WeaponMappingContext;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* FireAction;

	UPROPERTY(EditAnywhere, BlueprintReadOnly, Category=Input)
	UInputAction* ReloadAction;


	UPROPERTY(BlueprintReadOnly, Category=Gameplay)
	int32 MaxAmmoCount = 10;

	UPROPERTY(BlueprintReadOnly, Category=Gameplay)
	int32 AmmoCount = MaxAmmoCount;

private:
	UPROPERTY()
	AShooterCharacter* Character;
};
