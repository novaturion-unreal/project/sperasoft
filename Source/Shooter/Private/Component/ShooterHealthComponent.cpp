// Fill out your copyright notice in the Description page of Project Settings.


#include "Component/ShooterHealthComponent.h"

// Sets default values for this component's properties
UShooterHealthComponent::UShooterHealthComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true;

	// ...
}


// Called when the game starts
void UShooterHealthComponent::BeginPlay()
{
	Super::BeginPlay();

	// ...
}


// Called every frame
void UShooterHealthComponent::TickComponent(
	float DeltaTime,
	ELevelTick TickType,
	FActorComponentTickFunction* ThisTickFunction
)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

float UShooterHealthComponent::GetHealth() const
{
	return Health;
}

float UShooterHealthComponent::DecreaseHealth(
	float Amount
)
{
	if (bIsImmortal)
	{
		return Health;
	}

	Health = FMath::Clamp(Health - Amount, 0.0f, MaxHealth);
	return Health;
}

float UShooterHealthComponent::IncreaseHealth(
	float Amount
)
{
	if (bIsImmortal)
	{
		return Health;
	}

	Health = FMath::Clamp(Health + Amount, 0.0f, MaxHealth);
	return Health;
}
