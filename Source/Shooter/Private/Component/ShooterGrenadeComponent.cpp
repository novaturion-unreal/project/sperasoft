﻿#include "Component/ShooterGrenadeComponent.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "Character/ShooterCharacter.h"

#include "Components/SphereComponent.h"

#include "GameFramework/ProjectileMovementComponent.h"

#include "Kismet/GameplayStatics.h"

#include "Projectile/ShooterGrenade.h"


UShooterGrenadeComponent::UShooterGrenadeComponent() {}

void UShooterGrenadeComponent::Attach(
	AShooterCharacter* TargetCharacter
)
{
	if (!TargetCharacter)
	{
		return;
	}

	ToggleVisibility();

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	AttachToComponent(TargetCharacter->GetFirstPersonMeshComponent(), AttachmentRules);

	auto PlayerController = Cast<APlayerController>(TargetCharacter->GetController());
	if (!PlayerController)
	{
		return;
	}

	const auto Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(
		PlayerController->GetLocalPlayer()
	);
	if (Subsystem)
	{
		Subsystem->AddMappingContext(GrenadeMappingContext, 1);
	}

	const auto EnhancedInputComponent = Cast<UEnhancedInputComponent>(
		PlayerController->InputComponent
	);
	if (!EnhancedInputComponent)
	{
		return;
	}

	EnhancedInputComponent->BindAction(
		ThrowAction,
		ETriggerEvent::Started,
		this,
		&UShooterGrenadeComponent::PredictTrajectory
	);

	EnhancedInputComponent->BindAction(
		ThrowAction,
		ETriggerEvent::Completed,
		this,
		&UShooterGrenadeComponent::Throw
	);
}

void UShooterGrenadeComponent::PredictTrajectory()
{
	if (!Projectile)
	{
		return;
	}

	const auto PredictParams = FPredictProjectilePathParams(
		Projectile->GetCollisionComponent()->GetScaledSphereRadius(),
		GetComponentLocation(),
		GetForwardVector() * Projectile->GetProjectileMovementComponent()->InitialSpeed,
		5.0f,
		ECollisionChannel::ECC_Visibility
	);
	FPredictProjectilePathResult PredictResult;
	UGameplayStatics::PredictProjectilePath(GetWorld(), PredictParams, PredictResult);
}

void UShooterGrenadeComponent::Throw()
{
	// if (!Projectile)
	// {
	// 	return;
	// }

	const auto Owner = GetOwner();
	// EnableGrenade();

	auto PlayerController = Cast<APlayerController>(GetWorld()->GetFirstPlayerController());
	auto Rotation = PlayerController->PlayerCameraManager->GetCameraRotation();

	auto ActorSpawnParams = FActorSpawnParameters();
	ActorSpawnParams.SpawnCollisionHandlingOverride =
		ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

	Projectile = GetWorld()->SpawnActor<AShooterGrenade>(
		ProjectileClass,
		Owner->GetActorLocation() + Rotation.RotateVector(HandOffset),
		Rotation,
		ActorSpawnParams
	);

	// auto TimerHandle = FTimerHandle();
	// GetOwner()->GetWorldTimerManager().SetTimer(
	// 	TimerHandle,
	// 	this,
	// 	&UShooterGrenadeComponent::ExplodeGrenade,
	// 	3.0f
	// );
}

void UShooterGrenadeComponent::EnableGrenade()
{
	if (!Projectile)
	{
		return;
	}

	Projectile->SetActorTickEnabled(false);
	Projectile->SetActorEnableCollision(false);
	Projectile->SetActorHiddenInGame(true);
}

void UShooterGrenadeComponent::DisableGrenade()
{
	if (!Projectile)
	{
		return;
	}

	Projectile->SetActorTickEnabled(true);
	Projectile->SetActorEnableCollision(true);
	Projectile->SetActorHiddenInGame(false);
}

void UShooterGrenadeComponent::ExplodeGrenade()
{
	if (!Projectile)
	{
		return;
	}

	// Projectile->Explode();
	// DisableGrenade();
}

void UShooterGrenadeComponent::BeginPlay()
{
	Super::BeginPlay();

	// Projectile = Cast<AShooterGrenade>(GetWorld()->SpawnActor(ProjectileClass));
	// UShooterGrenadeComponent::DisableGrenade();

	// SetStaticMesh(
	// 	Projectile
	// 	? Projectile->GetMeshComponent()->GetStaticMesh()
	// 	: nullptr
	// );
}
