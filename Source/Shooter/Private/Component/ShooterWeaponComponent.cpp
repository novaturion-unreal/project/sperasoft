// Copyright Epic Games, Inc. All Rights Reserved.


#include "Component/ShooterWeaponComponent.h"

#include "EnhancedInputComponent.h"
#include "EnhancedInputSubsystems.h"

#include "Camera/PlayerCameraManager.h"

#include "Character/ShooterCharacter.h"

#include "GameFramework/PlayerController.h"

#include "Kismet/GameplayStatics.h"

#include "Projectile/ShooterProjectile.h"


UShooterWeaponComponent::UShooterWeaponComponent()
{
	// Default offset from the character location for projectiles to spawn
	MuzzleOffset = FVector(100.0f, 0.0f, 10.0f);
}

int32 UShooterWeaponComponent::GetAmmoCount() const
{
	return AmmoCount;
}

int32 UShooterWeaponComponent::GetMaxAmmoCount() const
{
	return MaxAmmoCount;
}


void UShooterWeaponComponent::Fire()
{
	if (!Character || !Character->GetController() || AmmoCount <= 0)
	{
		return;
	}

	if (ProjectileClass)
	{
		UWorld* const World = GetWorld();
		if (World)
		{
			APlayerController* PlayerController = Cast<APlayerController>(Character->GetController());
			const FRotator SpawnRotation = PlayerController->PlayerCameraManager->GetCameraRotation();
			auto Actor = GetOwner();
			// MuzzleOffset is in camera space, so transform it to world space before offsetting from the character location to find the final muzzle position
			const FVector SpawnLocation = Actor->GetActorLocation() + SpawnRotation.RotateVector(MuzzleOffset);

			//Set Spawn Collision Handling Override
			FActorSpawnParameters ActorSpawnParams;
			ActorSpawnParams.SpawnCollisionHandlingOverride =
				ESpawnActorCollisionHandlingMethod::AdjustIfPossibleButDontSpawnIfColliding;

			// Spawn the projectile at the muzzle
			World->SpawnActor<AShooterProjectile>(ProjectileClass, SpawnLocation, SpawnRotation, ActorSpawnParams);

			--AmmoCount;
		}
	}

	// Try and play the sound if specified
	if (FireSound)
	{
		UGameplayStatics::PlaySoundAtLocation(this, FireSound, Character->GetActorLocation());
	}

	// Try and play a firing animation if specified
	if (FireAnimation)
	{
		// Get the animation object for the arms mesh
		UAnimInstance* AnimInstance = Character->GetFirstPersonMeshComponent()->GetAnimInstance();
		if (AnimInstance)
		{
			AnimInstance->Montage_Play(FireAnimation, 1.f);
		}
	}
}

void UShooterWeaponComponent::Reload()
{
	AmmoCount = 10;
}

void UShooterWeaponComponent::Attach(
	AShooterCharacter* TargetCharacter
)
{
	Character = TargetCharacter;
	if (Character == nullptr)
	{
		return;
	}

	FAttachmentTransformRules AttachmentRules(EAttachmentRule::SnapToTarget, true);
	AttachToComponent(Character->GetFirstPersonMeshComponent(), AttachmentRules, FName(TEXT("GripPoint")));

	Character->SetHasRifle(true);

	APlayerController* PlayerController = Cast<APlayerController>(Character->GetController());
	if (!PlayerController)
	{
		return;
	}

	const auto Subsystem = ULocalPlayer::GetSubsystem<UEnhancedInputLocalPlayerSubsystem>(
		PlayerController->GetLocalPlayer()
	);
	if (Subsystem)
	{
		// Set the priority of the mapping to 1, so that it overrides
		// the Jump action with the Fire action when using touch input
		Subsystem->AddMappingContext(WeaponMappingContext, 1);
	}

	const auto EnhancedInputComponent = Cast<UEnhancedInputComponent>(
		PlayerController->InputComponent
	);
	if (!EnhancedInputComponent)
	{
		return;
	}

	EnhancedInputComponent->BindAction(
		FireAction,
		ETriggerEvent::Triggered,
		this,
		&UShooterWeaponComponent::Fire
	);

	EnhancedInputComponent->BindAction(
		ReloadAction,
		ETriggerEvent::Triggered,
		this,
		&UShooterWeaponComponent::Reload
	);
}

void UShooterWeaponComponent::EndPlay(
	const EEndPlayReason::Type EndPlayReason
)
{
	if (Character == nullptr)
	{
		return;
	}

	if (APlayerController* PlayerController = Cast<APlayerController>(Character->GetController()))
	{
		if (UEnhancedInputLocalPlayerSubsystem* Subsystem = ULocalPlayer::GetSubsystem<
			UEnhancedInputLocalPlayerSubsystem>(PlayerController->GetLocalPlayer()))
		{
			Subsystem->RemoveMappingContext(WeaponMappingContext);
		}
	}
}
