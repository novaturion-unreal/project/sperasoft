﻿#include "Weapon/ShooterPickupWeapon.h"

#include "Component/ShooterPickupComponent.h"
#include "Component/ShooterWeaponComponent.h"


AShooterPickupWeapon::AShooterPickupWeapon()
{
	PickUpComponent = CreateDefaultSubobject<UShooterPickUpComponent>(TEXT("PickUpComponent"));
	WeaponComponent = CreateDefaultSubobject<UShooterWeaponComponent>(TEXT("WeaponComponent"));

	PickUpComponent->SetupAttachment(WeaponComponent);
}

void AShooterPickupWeapon::BeginPlay()
{
	Super::BeginPlay();

	PickUpComponent->OnPickUp.AddDynamic(WeaponComponent, &UShooterWeaponComponent::Attach);
}
