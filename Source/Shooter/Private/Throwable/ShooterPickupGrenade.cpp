﻿#include "Throwable/ShooterPickupGrenade.h"

#include "Component/ShooterGrenadeComponent.h"
#include "Component/ShooterPickupComponent.h"


AShooterPickupGrenade::AShooterPickupGrenade()
{
	PickUpComponent = CreateDefaultSubobject<UShooterPickUpComponent>(TEXT("PickUpComponent"));
	GrenadeComponent = CreateDefaultSubobject<UShooterGrenadeComponent>(TEXT("GrenadeComponent"));

	PickUpComponent->SetupAttachment(GrenadeComponent);
}

void AShooterPickupGrenade::BeginPlay()
{
	Super::BeginPlay();

	PickUpComponent->OnPickUp.AddDynamic(GrenadeComponent, &UShooterGrenadeComponent::Attach);
}
