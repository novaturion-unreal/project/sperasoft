﻿#include "Projectile/ShooterBullet.h"

#include "Components/SphereComponent.h"


AShooterBullet::AShooterBullet()
{
	CollisionComponent->OnComponentHit.AddDynamic(this, &AShooterBullet::OnHit);
}

void AShooterBullet::OnHit(
	UPrimitiveComponent* HitComp,
	AActor* OtherActor,
	UPrimitiveComponent* OtherComp,
	FVector NormalImpulse,
	const FHitResult& Hit
)
{
	if (OtherActor && OtherActor != this && OtherComp && OtherComp->IsSimulatingPhysics())
	{
		OtherComp->AddImpulseAtLocation(GetVelocity(), GetActorLocation());
	}
	Destroy();
}
