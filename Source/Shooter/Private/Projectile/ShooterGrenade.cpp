﻿#include "Projectile/ShooterGrenade.h"

#include "PhysicsEngine/RadialForceComponent.h"


AShooterGrenade::AShooterGrenade()
{
	RadialForceComponent = CreateDefaultSubobject<URadialForceComponent>(TEXT("RadialForceComponent"));
	RadialForceComponent->AddCollisionChannelToAffect(ECollisionChannel::ECC_Destructible);
	RadialForceComponent->AddCollisionChannelToAffect(ECollisionChannel::ECC_Pawn);
	RadialForceComponent->AddCollisionChannelToAffect(ECollisionChannel::ECC_PhysicsBody);
	RadialForceComponent->AddCollisionChannelToAffect(ECollisionChannel::ECC_Vehicle);
	RadialForceComponent->AddCollisionChannelToAffect(ECollisionChannel::ECC_WorldDynamic);
	RadialForceComponent->SetupAttachment(RootComponent);

	InitialLifeSpan = 3.0f;
}

void AShooterGrenade::Destroyed()
{
	RadialForceComponent->FireImpulse();
	// Destroy();
}
