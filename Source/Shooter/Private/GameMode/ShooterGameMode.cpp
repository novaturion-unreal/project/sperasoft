// Copyright Epic Games, Inc. All Rights Reserved.

#include "GameMode/ShooterGameMode.h"

#include "Character/ShooterCharacter.h"

#include "GameFramework/HUD.h"

#include "UObject/ConstructorHelpers.h"

AShooterGameMode::AShooterGameMode() :
	Super()
{
	DefaultPawnClass = AShooterCharacter::StaticClass();
	HUDClass = ConstructorHelpers::FClassFinder<AHUD>(TEXT("/Game/Shooter/HUD/BP_Hud.BP_Hud_C")).Class;
}
